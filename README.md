# trm-mas

## Requisitos

*  [Java (8+)](https://www.oracle.com/technetwork/pt/java/javase/downloads/jdk8-downloads-2133151.html)
*  [Maven](https://maven.apache.org/)

## Arquitetura de comunicação dos agentes

<img src="readme/arquitetura_comunicacao_implementacional.png" width="600" height="550" />

## Arquitetura da aplicação

<img src="readme/arquitetura_implantacao.png" width="600" height="500" />
