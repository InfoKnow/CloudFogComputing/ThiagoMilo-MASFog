package br.com.tm.util;

public class Configuration {
	/**
	 * Parameters for simulation and tests 
	 */
	public static final long SECOND = 1000; 
	public static final long MINUTE = SECOND; //Adjusting - means that 1 MINUTE == 1 SECOND
	public static final long HOUR = MINUTE;
	public static final long DAY = 24 * HOUR; 
	
	public static final int SIMULATION_INIT_DELAY = 6000; //5 minutos
	public static final int DEFAULT_SERVICE_PORT = 9876;
	
	public static final int RANDOM = 0;
	public static final int TRUST = 1;
	public static final int TRUST_AND_REPUTATION = 2;
	
	public static final int NUM_EXPERIMENT_CYCLES = 6;
	
	public static final int MINIMUN_LATENCY_THRESHOLD = 30;
	
	public static final double LOCAL_COORDINATOR_DISTANCE_LIMIT = 2000.00; //in km
	
	public static final long SUPERNODES_CHECK_TIME_WINDOW = DAY;
	
	public static final FogServiceModel GENERAL_SERVICE_MODEL = FogServiceModel.PARTIALLY_OPEN;
	
}
