package br.com.tm.util;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadLocalRandom;

public class AgentA implements Runnable {

	private final BlockingQueue<String> queue;
	protected int val;
	protected Boolean flag;
	
	public AgentA(BlockingQueue<String> q) {
		this.queue = q;
	}
	
	public AgentA(BlockingQueue<String> q, int val, Boolean flag) {
		this.val = val;
		this.queue = q;
		this.flag = flag;
	}
	
	@Override
	public void run() {
		try {
			while(flag) {
				consume(queue.poll());
				doOtherThings();
			}
		} catch (InterruptedException e) {
			
		}
	}
	
	public void printTeste() {
		synchronized (flag) {
			System.out.println("TESTE FROM OTHER THREAD!!");
		}
		
	}
	
	public void consume(String value) {
		System.out.println(value);
	}
	
	private void doOtherThings() throws InterruptedException {
		System.out.println("Other things " + this.val);
		Thread.sleep(2000);
	}
	
}
