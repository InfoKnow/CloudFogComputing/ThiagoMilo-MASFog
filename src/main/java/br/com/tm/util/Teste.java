package br.com.tm.util;

import java.lang.reflect.InvocationTargetException;
import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.commons.math3.stat.regression.SimpleRegression;

import br.com.tm.agent.LocalCoordinator;
import br.com.tm.client.UDPClient;
import br.com.tm.cloudmanager.localcoordinator.LocalCoordinatorAgent;
import br.com.tm.node.NodeStatus;
import br.com.tm.repfogagent.util.StatisticUtil;
import br.com.tm.report.Report;
import br.com.tm.report.ReportFactory;
import jade.core.Agent;

public class Teste {	
	
	public static int iteration;

	public static void __main(String[] args) {
		int countDays = 1;
		Instant i = Instant.ofEpochMilli(new Date().getTime() + Configuration.HOUR);
		while(true) {
			System.out.println(countDays);
			while(Clock.tickSeconds(ZoneId.systemDefault()).instant().compareTo(i) < 0) { 	
			
			}
			countDays++;
			i = Instant.ofEpochMilli(new Date().getTime() + Configuration.HOUR);
		}
	}
	
	public static double calculateBootstrap() {
		double [] value = new double [] {0.7, 0.8};
		double [] influence = new double [] {0.3, 0.7};
		
		double overallValue = ((value[0] * influence[0]) + (value[1] * influence[1]));
		
		return overallValue;
	}
	
	public static double calculateLocalTrust() {
		double [] values = {0.9, 0.95, 0.98, 0.96, 0.97};
		double mean = StatisticUtil.meanRaw(Arrays.asList(0.9, 0.95, 0.98, 0.96, 0.97));
		double coefficient = StatisticUtil.coefficientVariationRaw(Arrays.asList(0.9, 0.95, 0.98, 0.96, 0.97)); 
		
		double total = 0;
		double denominator = 0;
		for(double v : values) {
			total += (1 - Math.abs(mean - v)) * v;
			denominator += (1- Math.abs(mean - v)); 
		}
		
		return (total / denominator) * Math.exp((-1.0/30.0) * coefficient);
	}
	
	protected static double utilityFunction(double x) {
		return 1 - 32 * Math.exp(-7 * x);
	}
	
	protected static double cloudUtilityFunction() {
		System.out.println(1 - (1 - Math.exp(-(110.0/45.0))) * 0.8);
		System.out.println(1 - (1 - Math.exp(-(100.0/45.0))) * 0.8);
		System.out.println(1 - (1 - Math.exp(-(90.0/45.0))) * 0.8);
		System.out.println(1 - (1 - Math.exp(-(30.0/45.0))) * 0.8);
		System.out.println(1 - (1 - Math.exp(-(15.0/45.0))) * 0.8);
		
		return 0;
	}
	
	public static void main(String[] args) {
		int requiredLatency = 50;
		int cloudMeanLatency = 70;
		double lotteryValue = 0;
		
		/*for(double x = 0.6; x < 1; x += 0.01) {
			System.out.println(utilityFunction(x));
		}
		
		System.out.println("\n\n");
		System.out.println(Math.log(1.75));*/
		
		//System.out.println(calculateBootstrap());
		
		//System.out.println(StatisticUtil.coefficientVariationRaw((Arrays.asList(0.8,0.4,0.5))));
		
		System.out.println(cloudUtilityFunction());
		
		String p = "grep -oP \'<ApplicationManifest .* ApplicationTypeVersion="\\K[^"]*\' nodeSample/nodeSample/ApplicationManifest.xml | sed -r \'s/\.[0-9]+$/.1/g\' | { read version; sed -r \'s/ApplicationTypeVersion\\s*=\\s*"([0-9]+|\.)+"/\'ApplicationTypeVersion="$version"\'/g\' ApplicationManifest.xml; }";
		
	}
	
	public Agent getAgent() {
		return new LocalCoordinatorAgent();
	}
	
	public static void _main(String[] args) {
		/*SimpleRegression regression = new SimpleRegression();
		regression.addData(1, 1);
		regression.addData(2, 2);
		regression.addData(3, 1.3);
		regression.addData(4, 3.75);
		regression.addData(5, 2.25);*/
		Object o = new Object();
		Thread clockThread = new Thread(new Runnable() {
			@Override
			public void run() {
				synchronized (o) {
					Instant i = Instant.ofEpochMilli(new Date().getTime() + 1000);
					while(true) {
						while(Clock.tickSeconds(ZoneId.systemDefault()).instant().compareTo(i) < 0) { 	
							System.out.println("RUNING");
						}
						i = Instant.ofEpochMilli(new Date().getTime() + 1000);
						try {
							o.notify();
							o.wait();
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}
		});
		clockThread.start();
		
		
		try {
			Thread.sleep(2000);
			clockThread.stop();
			System.out.println("Stoping");
			Thread.sleep(2000);
			System.out.println("Initing new");
			clockThread = new Thread(new Runnable() {
				@Override
				public void run() {
					synchronized (o) {
						Instant i = Instant.ofEpochMilli(new Date().getTime() + 1000);
						while(true) {
							while(Clock.tickSeconds(ZoneId.systemDefault()).instant().compareTo(i) < 0) { 	
								System.out.println("RUNING");
							}
							i = Instant.ofEpochMilli(new Date().getTime() + 1000);
							try {
								o.notify();
								o.wait();
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
				}
			});
			clockThread.start();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		//clockThread.notify();
		
		//System.out.println(regression.getSlope() + "X" + " + " + regression.getIntercept());
		
		/*Teste t = new Teste();
		BlockingQueue<String> q = new LinkedBlockingQueue<String>();
		AgentA agent = new AgentA(q);
		Client client = new Client(q);
		new Thread(agent).start();
		new Thread(client).start();
		
		System.out.println(NodeStatus.valueOf("CONNECTING"));*/
		
		/*Boolean flag = true;
		AgentA agent = new AgentA(q, 10, flag);
		Thread ta = new Thread(agent);
		ta.start();
		synchronized (flag) {
			try {
				Thread.sleep(5000);
				agent.printTeste();
				flag = false;
				Thread.sleep(60000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}*/
		
		
	}
	
}
