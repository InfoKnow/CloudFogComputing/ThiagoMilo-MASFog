package br.com.tm.main;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import br.com.tm.cloudmanager.CloudManagerAgent;
import br.com.tm.node.NodeAgent;
import jade.Boot;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.util.leap.ArrayList;
import jade.wrapper.AgentController;
import jade.wrapper.ContainerController;
import jade.wrapper.StaleProxyException;

public class Main {

	List<AgentController> agentList = new java.util.ArrayList<>();
	
	private ContainerController containerController;
	private String host;
	private String port;
	
	public Main() {
	}
	
	public Main(String host, String port) {
		this.host = host;
		this.port = port;
	}
	
	public void initJadePlatform(boolean useGui, boolean isMainContainer, int numClouds, int numNodes) throws InterruptedException {
		ContainerController cc;
		Boot.main(new String [] {useGui ? "-gui" : ""});
		jade.core.Runtime rt = jade.core.Runtime.instance();
		Profile p = new ProfileImpl();
		if(isMainContainer) {
			cc = rt.createMainContainer(p);
		} else {
			p.setParameter("host", this.host);
			p.setParameter("port", this.port);
			cc = rt.createAgentContainer(p);
		}
		setContainerController(cc);
		
		Thread.sleep(5000);
		for(int i = 0; i < numClouds; i++) {
			addAgentToContainer(CloudManagerAgent.class, "cloud", i);
		}
		
		Thread.sleep(5000);
		for(int i = 0; i < numNodes; i++) {
			addAgentToContainer(NodeAgent.class, "node", i);
		}
		
		for(AgentController agentController : this.agentList) {
			try {
				agentController.start();
				Thread.sleep(5000);
			} catch (StaleProxyException e) {
				e.printStackTrace();
			}
		}
		
		rt.setCloseVM(true);
	}
	
	private void addAgentToContainer(Class clazz, String baseName, int i) {
		try {
			AgentController a = getContainerController().createNewAgent(baseName + "-" + i, 
					clazz.getName(), null);
			agentList.add(a);
			//a.start();
		} catch (StaleProxyException e) {
			e.printStackTrace();
		}
	}
	
	public static Properties parseCmdLineArgs(String[] args) throws IllegalArgumentException {
		Properties props = new Properties();
		
		int i = 0;
		while(i < args.length) {
			if(args[i].startsWith("-")) {
				if(args[i].equalsIgnoreCase("-mainContainer")) {
					props.setProperty("mainContainer", "true");
				}
			}
		}
		
		return null;
	}
	
	public ContainerController getContainerController() {
		return containerController;
	}

	public void setContainerController(ContainerController containerController) {
		this.containerController = containerController;
	}
	
	public static void main(String[] args) {
		Main main = new Main();
		
		try {
			main.initJadePlatform(true, true, 1, 5);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
