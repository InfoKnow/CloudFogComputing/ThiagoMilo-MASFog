package br.com.tm.cloudmanager;

import java.io.IOException;

import org.apache.commons.lang3.SerializationUtils;

import br.com.tm.util.FogServiceModel;
import br.com.tm.util.Geolocation;
import br.com.tm.util.SupernodeRegistryEnvelop;
import jade.core.AID;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

public class SupernodeRegistrationServiceBehaviour extends CyclicBehaviour {

	private static final long serialVersionUID = 1L;

	private static final String SUPERNODE_REGISTRATION_CONVERSATION_ID = "supernode-registration-conversation-id";
	
	MessageTemplate mt = MessageTemplate.and(
			MessageTemplate.MatchConversationId(SUPERNODE_REGISTRATION_CONVERSATION_ID),
			MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_SUBSCRIBE)
	);

	private boolean isLocal;
	
	public SupernodeRegistrationServiceBehaviour(boolean isLocal) {
		this.isLocal = isLocal;
	}
	
	@Override
	public void action() {
		ACLMessage msg = myAgent.receive(mt);
		if(msg != null) {
			ACLMessage reply = msg.createReply();
			reply.setProtocol(FIPANames.InteractionProtocol.FIPA_SUBSCRIBE);
			switch (msg.getPerformative()) {
				case ACLMessage.SUBSCRIBE:
					if(getAgent().isAlive()) {
						try {
							reply.setPerformative(ACLMessage.AGREE);
							String params = msg.getContent();
							String [] paramsParts = params.split(";");
							int capacity = Integer.valueOf(paramsParts[0]);
							double lat = Double.valueOf(paramsParts[1]);
							double lon = Double.valueOf(paramsParts[2]);
							int port = Integer.valueOf(paramsParts[3]);
							String ip = paramsParts[4];
							
							getAgent().supernodesDatabase.addSupernode(msg.getSender(), new Geolocation(lat, lon), capacity, port, ip);
							
							if(getAgent().nodes.stream().filter(n -> n.equals(msg.getSender())).count() == 0) {
								getAgent().nodes.add(msg.getSender());
							}
							
							if(this.isLocal) {
								reply.setContent("ACK");
							} else {
								// Check if failure need to be introduced
								double pctToFail = getAgent().getSupernodeFailurePct();
								
								if(getAgent().fogServiceModel == FogServiceModel.FIXED || getAgent().fogServiceModel == FogServiceModel.PARTIALLY_OPEN) {
									AID localCoordinatorAid = getAgent().localCoordinatorDatabase.searchForNearestLocalCoordinator(new Geolocation(lat, lon));
									
									reply.setContentObject(new SupernodeRegistryEnvelop(pctToFail, getAgent().fogServiceModel.value, localCoordinatorAid));
								} else {
									reply.setContentObject(new SupernodeRegistryEnvelop(pctToFail, getAgent().fogServiceModel.value, null));
								}
									
							}
							
							System.out.println("Agreeing to " + msg.getSender().getLocalName() + " registry");
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					} else {
						reply.setPerformative(ACLMessage.REFUSE);
						System.out.println("Refusing supernode registry");
					}
				break;
	
				case ACLMessage.CANCEL:
					//Verify if also the agent should be remove as a node too
					if(msg.getContent() != null && msg.getContent() == Boolean.TRUE.toString()) {
						getAgent().nodes.remove(msg.getSender());
					}
					getAgent().supernodesDatabase.removeSupernode(msg.getSender());
				break;
			}
			
			myAgent.send(reply);
		} else {
			block();
		}
	}
	
	public CloudManagerAgent getAgent() {
		return (CloudManagerAgent) myAgent;
	}

}
