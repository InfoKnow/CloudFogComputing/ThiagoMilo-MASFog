package br.com.tm.cloudmanager.localcoordinator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import br.com.tm.cloudmanager.CloudManagerAgent;
import br.com.tm.cloudmanager.RatingDatabase;
import br.com.tm.cloudmanager.NodeRegistrationServiceBehaviour;
import br.com.tm.cloudmanager.SupernodeRegistrationServiceBehaviour;
import br.com.tm.cloudmanager.SupernodesDatabase;
import br.com.tm.commons.FogElementBehaviour;
import br.com.tm.util.Configuration;
import br.com.tm.util.Geolocation;
import jade.core.AID;

public class LocalCoordinatorAgent extends CloudManagerAgent {

	private static final long serialVersionUID = 1L;
	
	protected int failurePct; 
	protected int totalNodes;
	protected long simulationInitTime;
	protected int localTrustHistorySize = 7;
	protected double localTrust;
	protected int iteration;
	
	protected List<String> nodesToReceiveFailure;
	
	protected Geolocation centerLocation;
	protected int locationRadio;
	
	protected AID[] cloudManagers;
	
	protected Double latitude;
	protected Double longitude;
	
	@Override
	protected void setup() {
		supernodesDatabase = new SupernodesDatabase(Configuration.MINIMUN_LATENCY_THRESHOLD);
		nodes = new ArrayList<>();
		ratingDatabase = new RatingDatabase();
		nodesToReceiveFailure = new ArrayList<>();
		
		getParametersFromArguments("latitude", "longitude");
		
		if(latitude == null || longitude == null) {
			setUpLatLong();
		}
		
		addBehaviour(new LocalCoordinatorStartupBehaviour());
		addBehaviour(new LocalSearchServer());
		addBehaviour(new NodeRegistrationServiceBehaviour());
		addBehaviour(new SupernodeRegistrationServiceBehaviour(true));
		addBehaviour(new LocalTRMServerBehaviour());
		addBehaviour(new FogElementBehaviour());
	}
	
	// TODO Deeper checking - check if is inside region. If not suggests other.
	protected boolean checkIfBelongsRegion() {
		return true;
	}
	
	public void restartLocalCoordinator() {
		this.failurePct = 0;
	}
	
	protected void setUpLatLong() {
		try {
			URL urlIpGeolocation = new URL("http://ip-api.com/csv");
			URLConnection urlc = urlIpGeolocation.openConnection();

			urlc.setDoOutput(true);
			urlc.setConnectTimeout(90000);
			urlc.setReadTimeout(90000);
			
	        //get result
	        BufferedReader br = new BufferedReader(new InputStreamReader(urlc
	            .getInputStream()));
	        String l = null;
	        while ((l=br.readLine())!=null) {
	            String [] responseParts = l.split(",");
	            this.latitude = Double.parseDouble(responseParts[7]);
	            this.longitude = Double.parseDouble(responseParts[8]);
	        }
	        br.close();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NumberFormatException | NullPointerException e) {
			// TODO: handle exception
			System.out.println("Location CSV malformed - can't find lat/long");
		}

	}
	
	protected void getParametersFromArguments(String ... paramatersName) {
		if(getArguments() != null && getArguments().length > 0) {
			for(String parameter : paramatersName) {
				int i = 0;
				while(i < getArguments().length) {
					Object a = getArguments()[i]; 
					if(a instanceof String) {
						if(((String) a).equalsIgnoreCase(parameter)) {
							try {
								if(i + 1 < getArguments().length) {
									if(Arrays.stream(this.getClass().getDeclaredFields()).filter(f -> f.getName().equals(parameter)).count() > 0) {
										this.getClass().getDeclaredField(parameter).set(this, dynamicTypeSetting(this.getClass().getDeclaredField(parameter).getType(), getArguments()[i + 1].toString()));
									} else {
										System.out.println(this.getClass().getSuperclass().getName());
										this.getClass().getSuperclass().getDeclaredField(parameter).set(this, dynamicTypeSetting(this.getClass().getDeclaredField(parameter).getType(), getArguments()[i + 1].toString()));
									}
									
									i+=2;
									break;
								}
							} catch (NumberFormatException e) {
								throw new RuntimeException("Invalid arguments for capacity parameter");
							} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
								e.printStackTrace();
								throw new RuntimeException("Invalid arguments for capacity parameter");
							}
						}
					}
					i++;
				}
			}
		}
	}
	
	protected Object dynamicTypeSetting(Class fieldType, String value) {
		if(fieldType.getName().equals(Integer.class.getName()) || fieldType.getName().equals("int")) {
			return Integer.valueOf(value);
		} else if(fieldType.getName().equals(Long.class.getName()) || fieldType.getName().equals("long")) {
			return Long.valueOf(value);
		} else if(fieldType.getName().equals(Double.class.getName()) || fieldType.getName().equals("double")) {
			return Double.valueOf(value);
		}
		
		return null;
	}
}
