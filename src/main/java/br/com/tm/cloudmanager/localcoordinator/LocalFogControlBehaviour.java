package br.com.tm.cloudmanager.localcoordinator;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import br.com.tm.cloudmanager.SupernodeRecord;
import br.com.tm.repfogagent.trm.Rating;
import br.com.tm.repfogagent.util.StatisticUtil;
import br.com.tm.util.Configuration;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;

public class LocalFogControlBehaviour extends OneShotBehaviour {

	private static final long serialVersionUID = 1L;
	
	private static final String GET_NODE_DEVIATION_RATINGS = "get-node-deviation-ratings";
	
	private int iterationSpan = 3;
	
	@Override
	public void action() {
		controlGeneralTrust();
	}
	
	protected void controlGeneralTrust() {
		ACLMessage request = new ACLMessage(ACLMessage.REQUEST);
		request.setConversationId(GET_NODE_DEVIATION_RATINGS);
		request.setProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST);
		
		List<AID> nodes = getAgent().getNodes().stream().filter(n -> !n.getName().contains("super")).collect(Collectors.toList());
		for(AID node : nodes) {
			request.addReceiver(node);
		}
		request.setContent(getAgent().localTrustHistorySize + "");
		request.setReplyByDate(new Date(System.currentTimeMillis() + 3000));
		
		System.out.println("Sending requests for local trust to: ");
		nodes.forEach(n -> System.out.print(n.getLocalName() + " "));
		
		myAgent.addBehaviour(new UpdateLocalTrustBehaviour(myAgent, request));
	}
	
	protected void checkSupernodesServiceQuality() {
		int currentIteration = (int)((getAgent().simulationInitTime - new Date().getTime()) / Configuration.DAY);
		int iterationFilter = 0;
		if(currentIteration > iterationSpan) {
			iterationFilter = currentIteration - iterationSpan;
		}
		
		for(SupernodeRecord supernodeRecord : getAgent().getSupernodesDatabase().getSupernodes()) {
			Map<String, List<Rating>> ratingsForSupernode = new HashMap<>();
			for(Entry<String, List<Rating>> entry : getAgent().getRatingDatabase().recoverForServer(supernodeRecord.supernodeAID.getName()).entrySet()) {
				List<Rating> rs = new ArrayList<>();
				for(Rating r : entry.getValue()) {
					if(r.getIteration() > iterationFilter) {
						rs.add(r);	
					}
				}
				
				if(!rs.isEmpty()) {
					ratingsForSupernode.put(entry.getKey(), rs);
				}
			}
		}
			
	}
	
	protected void checkSupernodesWatchList() {
		
	}
	
	public LocalCoordinatorAgent getAgent() {
		return (LocalCoordinatorAgent) myAgent;
	}
	
}
