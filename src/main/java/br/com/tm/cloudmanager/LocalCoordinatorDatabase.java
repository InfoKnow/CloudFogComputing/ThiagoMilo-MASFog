package br.com.tm.cloudmanager;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import br.com.tm.util.Geolocation;
import br.com.tm.util.Util;
import jade.core.AID;

public class LocalCoordinatorDatabase {

	private Map<String, LocalCoordinatorRecord> localCoordinators;
	private Map<String, List<String>> mapLocalCoordinatorsNodes;
	private Double distanceLimit;
	
	public LocalCoordinatorDatabase(double distanceLimit) {
		this.distanceLimit = new Double(distanceLimit);
		this.localCoordinators = new HashMap<>();
		this.mapLocalCoordinatorsNodes = new HashMap<>();
	}
	
	public void addLocalCoordinator(AID localCoordinatorAID, Geolocation geolocation, int capacity) {
		this.localCoordinators.put(localCoordinatorAID.getName(), new LocalCoordinatorRecord(localCoordinatorAID, geolocation, capacity));
	}
	
	public void removeLocalCoordinator(AID localCoordinatorAID) {
		this.localCoordinators.remove(localCoordinatorAID.getName());
	}
	
	public void addNodeToLocalCoordinator(String localCoordinator, String node) {
		mapLocalCoordinatorsNodes.putIfAbsent(localCoordinator, new ArrayList<>());
		mapLocalCoordinatorsNodes.get(localCoordinator).add(node);
	}
	
	public void removeNodeFromLocalCoordinator(String localCoordinator, String node) {
		if(mapLocalCoordinatorsNodes.get(localCoordinator) != null) {
			mapLocalCoordinatorsNodes.get(localCoordinator).remove(node);
		}
	}
	
	public AID searchForNearestLocalCoordinator(Geolocation requesterGeolocation) {
		if(localCoordinators != null && !localCoordinators.isEmpty()) {
			Map<String, Double> distMap = new HashMap();
			
			for(Entry<String, LocalCoordinatorRecord> le : localCoordinators.entrySet()) {
				Double dist = distance(le.getValue().geolocation, requesterGeolocation);
				distMap.put(le.getKey(), dist);
			}
			
			Entry<String, Double> nearestEntry = distMap.entrySet().stream().sorted(new Comparator<Entry<String,Double>>() {
				@Override
				public int compare(Entry<String, Double> o1, Entry<String, Double> o2) {
					return o1.getValue().compareTo(o2.getValue());
				}
			}).findFirst().orElse(null);
			
			if(nearestEntry != null && nearestEntry.getValue().compareTo(distanceLimit) <= 0) {
				return localCoordinators.get(nearestEntry.getKey()).supernodeAID;
			}
		}
		return null;
	}
	
	public List<String> getNodesPerLocalCoordinator(String localCoordinator) {
		return mapLocalCoordinatorsNodes.get(localCoordinator);
	}
	
	public List<String> getLocalCoordinators() {
		return new ArrayList<String>(localCoordinators.keySet());
	}
	
	public List<AID> getLocalCoordinatorsAID() {
		return localCoordinators.values().stream().map(l -> l.supernodeAID).collect(Collectors.toList());
	}
	
	public AID getLocalCoordinator(String name) {
		LocalCoordinatorRecord lcRecord = localCoordinators.values().stream().filter(l -> l.supernodeAID.getName().equalsIgnoreCase(name)).findFirst().orElse(null);
		if(lcRecord != null) {
			return lcRecord.supernodeAID;
		}
		
		return null;
	}
	
	/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	/*::  Geolocation functions                                         :*/
	/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	public double distance(Geolocation geolocationA, Geolocation geolocationB) {
		return distance(geolocationA.getLatitude(), geolocationA.getLongitude(), geolocationB.getLatitude(), geolocationB.getLongitude(), 'K');
	}
	
	public double distance(double lat1, double lon1, double lat2, double lon2, char unit) {
		double theta = lon1 - lon2;
		double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
		dist = Math.acos(dist);
		dist = rad2deg(dist);
		dist = dist * 60 * 1.1515;
		if (unit == 'K') {
			dist = dist * 1.609344;
		} else if (unit == 'N') {
			dist = dist * 0.8684;
		}
		return dist;
	}
	/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	/*::  This function converts decimal degrees to radians             :*/
	/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	private double deg2rad(double deg) {
		return (deg * Math.PI / 180.0);
	}
	/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	/*::  This function converts radians to decimal degrees             :*/
	/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	private double rad2deg(double rad) {
		return (rad * 180.0 / Math.PI);
	}
	
	public int kmToLatencyRatio(double distInKm) { 
		return (int)Math.round(1000*((distInKm * 1000) / 1.85e8));  
	}
	
	public double latencyToKmRatio(int latencyInMs) {
		return (1.85e8 * latencyInMs) / (1000000);
	}
	
	
	class LocalCoordinatorRecord {
		public AID supernodeAID;
		public Geolocation geolocation;
		public int capacity;
		
		public LocalCoordinatorRecord(AID supernodeAID, Geolocation geolocation, int capacity) {
			super();
			this.supernodeAID = supernodeAID;
			this.geolocation = geolocation;
			this.capacity = capacity;
		}
	}
	
}
