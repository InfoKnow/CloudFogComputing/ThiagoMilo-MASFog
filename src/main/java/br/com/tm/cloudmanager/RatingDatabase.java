package br.com.tm.cloudmanager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import br.com.tm.repfogagent.trm.Rating;
import jade.core.AID;

public class RatingDatabase {

	private int ratingHistorySize;
	
	List<LocalRatingRecord> records;
	
	public RatingDatabase() {
		this.records = new ArrayList<>();
	}
	
	public RatingDatabase(int ratingHistorySize) {
		this.ratingHistorySize = ratingHistorySize;
	}
	
	public void insert(AID node, AID server, List<Rating> ratings) {
		int index = IntStream.range(0, records.size()).filter(i -> records.get(i).node.equals(node) && records.get(i).server.equals(server)).findFirst().orElse(-1);
		if(index >= 0) {
			records.get(index).addRatings(ratings);
		} else {
			LocalRatingRecord lc = new LocalRatingRecord(node, server);
			lc.addRatings(ratings);
			records.add(lc);
		}
	}
	
	public List<Rating> recover(String node, String server) {
		return records.stream().filter(r -> r.node.equals(node) && r.server.equals(server))
							   .findFirst()
							   .map(r -> r.ratings)
							   .orElse(null);
	}
	
	public List<AID> recoverReferralsForServer(String serverName) {
		return records.stream().filter(r -> r.server.getName()
							   .equals(serverName))
				               .map(r -> r.node)
				               .collect(Collectors.toList());
	}
	
	public Map<String, List<Rating>> recoverForServer(String server) {
		Map<String, List<Rating>> result = new HashMap<>();
		for(LocalRatingRecord lr : records) {
			if(lr.server.equals(server)) {
				result.put(lr.node.getName(), lr.ratings);
			}
		}
		
		return result;
	}
	
	/**
	 * 
	 * @author milovaz
	 *
	 */
	class LocalRatingRecord {
		public AID node;
		public AID server;
		public List<Rating> ratings;
		
		public LocalRatingRecord(AID node, AID server) {
			this.node = node;
			this.server = server;		
		}
		
		public void addRatings(List<Rating> newRatings) {
			if(newRatings != null) {
				if(ratings == null) {
					ratings = new ArrayList<>();
				}

				ratings.addAll(newRatings);
			}
		}
	}
	
}
