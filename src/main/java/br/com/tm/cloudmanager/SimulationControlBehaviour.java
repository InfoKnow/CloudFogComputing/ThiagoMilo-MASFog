package br.com.tm.cloudmanager;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import br.com.tm.node.NodeAgent;
import br.com.tm.report.Report;
import br.com.tm.report.ReportFactory;
import br.com.tm.util.Configuration;
import br.com.tm.util.SimulatorGen;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class SimulationControlBehaviour extends Behaviour {

	private static final long serialVersionUID = 1L;
	
	private static final String SIMULATION_STATS_CONVERSATION = "sim-stats-certificate";
	private static final String SIMULATION_ITERATION_CONTROL_CONVERSATION = "sim-iteration-control";
	
	MessageTemplate mtSimulationStats = MessageTemplate.and(
			MessageTemplate.MatchConversationId(SIMULATION_STATS_CONVERSATION),
			MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_QUERY)
	);
	
	MessageTemplate mtSimulationIterationControl = MessageTemplate.and(
			MessageTemplate.MatchConversationId(SIMULATION_ITERATION_CONTROL_CONVERSATION),
			MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST)
	);

	private static final String SIMULATION_PARAMAS_CONVERSATION_ID = "simulation-params-conversation-id";
	
	private static final double MINIMUN_NODE_NET_PCT_SIZE = 1;
	
	private static final int GENERATING_SIMULATION_PARAMS = 0;
	private static final int WAITING_FOR_NODES_CONNECTION = 1;
	private static final int SENDING_SIMULATION_PARAMS = 2;
	private static final int CONTROL_ITERATIONS = 3;

	private int step = GENERATING_SIMULATION_PARAMS;
	
	
	public SimulationControlBehaviour() {
	}
	
	@Override
	public void action() {
		switch (step) {
			case GENERATING_SIMULATION_PARAMS:
				getAgent().numIterationResponses = 0;
				myAgent.addBehaviour(new SimulationDataServiceBehaviour());
				step = WAITING_FOR_NODES_CONNECTION;
			break;
	
			case WAITING_FOR_NODES_CONNECTION:
				System.out.println(getAgent().nodes.size());
				if(getAgent().nodes.size() >= (getAgent().numNodes * MINIMUN_NODE_NET_PCT_SIZE)) { 
					getAgent().simulationStarted = true;
					step = SENDING_SIMULATION_PARAMS;
				} else {
					block(1000);
				}
			break;
			
			case SENDING_SIMULATION_PARAMS:
				ACLMessage requestServiceMessage;
				
				long simulationInitTime = new Date().getTime() + 15000; //Data atual + 1 minuto
				
				getAgent().loadSimulationParams();
				
				for(int i = 0; i < ((CloudManagerAgent) myAgent).nodes.size(); i++) {
					AID node = ((CloudManagerAgent) myAgent).nodes.get(i);
					requestServiceMessage = new ACLMessage(ACLMessage.INFORM);
					requestServiceMessage.setConversationId(SIMULATION_PARAMAS_CONVERSATION_ID);
					requestServiceMessage.setProtocol(FIPANames.InteractionProtocol.FIPA_SUBSCRIBE);
					requestServiceMessage.addReceiver(node);
					
					long [] simulationParams = getAgent().getParamsForNode();
					
					//Simulation init time; execution start init; execution end init; duration
					String params =  simulationInitTime + ";" + simulationParams[0] + ";" + simulationParams[1] + ";" + simulationParams[2] + ";" + getAgent().experimentCycles + ";" + getAgent().iteration;
					System.out.println(params);
					requestServiceMessage.setContent(params);
					
					System.out.println("Sending simulation params to " + node.getName());
					
					myAgent.send(requestServiceMessage);
				}
				
				List<AID> localCoordinators = ((CloudManagerAgent) myAgent).localCoordinatorDatabase.getLocalCoordinatorsAID(); 
				Collections.shuffle(localCoordinators);
				for(AID lc : localCoordinators) {
					ACLMessage msgToLC = new ACLMessage(ACLMessage.INFORM);
					msgToLC.setConversationId(SIMULATION_PARAMAS_CONVERSATION_ID);
					msgToLC.setProtocol(FIPANames.InteractionProtocol.FIPA_SUBSCRIBE);
					msgToLC.addReceiver(lc);
					String content = "";
					content += ((getAgent().iteration % 7) < 4) ? getAgent().getLocalCoordinatorFailurePct() : 0.0;
					content += ";" + 0;//getAgent().localCoordinatorDatabase.getNodesPerLocalCoordinator(lc.getName()).size(); 
					content += ";" + getAgent().iteration;
					msgToLC.setContent(content);
					
					myAgent.send(msgToLC);
				}

				getAgent().simulationStartTime = simulationInitTime;
				getAgent().simulationReport.add("START TIME;TOTAL CYCLES;HOUR DEFINITION");
				getAgent().simulationReport.add(simulationInitTime + ";" + getAgent().experimentCycles + ";" + Configuration.HOUR);
				
				step = CONTROL_ITERATIONS;
			break;
			
			case CONTROL_ITERATIONS:
				ACLMessage msgSimControl = myAgent.receive(mtSimulationIterationControl);
				if(msgSimControl != null) {
					String content = msgSimControl.getContent();
					ACLMessage reply = msgSimControl.createReply();
					reply.setContent("ACK");
					reply.setPerformative(ACLMessage.AGREE);
					myAgent.send(reply);
					
					getAgent().numIterationResponses++;
					
					System.out.println("Received control iteration message " + msgSimControl.getSender().getLocalName() + " - " + getAgent().numIterationResponses);
					
					if(getAgent().numIterationResponses >= (getAgent().nodes.size() - getAgent().supernodesDatabase.count())) {
						getAgent().iteration++;
						iterationDigest();
						getAgent().numIterationResponses = 0;
						getAgent().loadSimulationParams();
						step = SENDING_SIMULATION_PARAMS;
					}
					
				} else {
					block();
				}
			break;
		}
	}
	
	private void iterationDigest() {
		if(getAgent().iteration > 0 && 
		  (getAgent().iteration % getAgent().iterationCheckPointPatter == 0)) {
			for(String localCoord : getAgent().localCoordinatorDatabase.getLocalCoordinators()) {
				getAgent().addBehaviour(new CheckLocalCoordinatorQualityBehaviour(localCoord));
			}
		}
	}
	
	public CloudManagerAgent getAgent() {
		return (CloudManagerAgent) myAgent;
	}
	
	@Override
	public boolean done() {
		return false;
	}
}
