package br.com.tm.cloudmanager;

import java.util.Enumeration;
import java.util.Vector;

import jade.core.AID;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.proto.ContractNetInitiator;

public class ChangeLocalCoordinatorBehaviour extends OneShotBehaviour {

	private static final long serialVersionUID = 1L;
	
	private static final String CFP_LOCAL_COORDINATORS = "cfp-local-coordinators";
	
	private static final int SEND_CFPS = 0;
	private static final int RECEIVE_PROPOSALS = 1;
	
	private int step = SEND_CFPS;
	
	private AID targetLocalCoordinator;
	
	private double proposalValueLimit = 200;
	
	public ChangeLocalCoordinatorBehaviour(AID localCoordinator) {
		this.targetLocalCoordinator = localCoordinator;
	}
	
	@Override
	public void action() {
		
		System.out.println("Changing Local Coordinator");
		
		ACLMessage msg = new ACLMessage(ACLMessage.CFP);
		msg.setConversationId(CFP_LOCAL_COORDINATORS);
		msg.setProtocol(FIPANames.InteractionProtocol.FIPA_CONTRACT_NET);
		msg.setContent(targetLocalCoordinator.getName());
		msg.addReceiver(this.targetLocalCoordinator);
		
		myAgent.addBehaviour(new ContractNetInitiator(myAgent, msg) {
			@Override
			protected void handleAllResponses(Vector responses, Vector acceptances) {
				int bestProposal = Integer.MAX_VALUE;
				AID bestProposer = null;
				ACLMessage accept = null;
				
				Enumeration e = responses.elements();
				while(e.hasMoreElements()) {
					ACLMessage msg = (ACLMessage) e.nextElement();
					if (msg.getPerformative() == ACLMessage.PROPOSE) {
						ACLMessage reply = msg.createReply();
						reply.setPerformative(ACLMessage.REJECT_PROPOSAL);
						acceptances.addElement(reply);
						int proposal = Integer.parseInt(msg.getContent());
						if (proposal < bestProposal) {
							bestProposal = proposal;
							bestProposer = msg.getSender();
							accept = reply;
						}
					}
				}
				
				if(accept != null) {
					System.out.println("Accepting proposal "+bestProposal+" from responder "+bestProposer.getName());
					accept.setPerformative(ACLMessage.ACCEPT_PROPOSAL);
				}
				
				myAgent.send(accept);
			}
		});
	}
}
