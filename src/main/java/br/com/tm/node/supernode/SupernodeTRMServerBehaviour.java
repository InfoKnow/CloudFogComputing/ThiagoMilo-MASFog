package br.com.tm.node.supernode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import FIPA.FipaMessage;
import br.com.tm.node.NodeAgent;
import br.com.tm.repfogagent.trm.Rating;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

public class SupernodeTRMServerBehaviour extends CyclicBehaviour {

	private static final long serialVersionUID = 1L;

	private static final String GET_SUPERNODE_CERTIFICATE_CONVERSATION = "get-supernode-certificate";
	private static final String RATING_TO_SUPERNODE_ID = "rating-to-supernode";
	
	MessageTemplate mtTrm = MessageTemplate.and(
		MessageTemplate.and(	
			MessageTemplate.or(
					MessageTemplate.MatchConversationId(GET_SUPERNODE_CERTIFICATE_CONVERSATION), 
					MessageTemplate.MatchConversationId(RATING_TO_SUPERNODE_ID)),
			MessageTemplate.or(
					MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST),
					MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_QUERY))
		),
		MessageTemplate.or(
				MessageTemplate.MatchPerformative(ACLMessage.REQUEST),
				MessageTemplate.MatchPerformative(ACLMessage.QUERY_REF)
		)
	);
	
	@Override
	public void action() {
		ACLMessage msg = myAgent.receive(mtTrm);
		if(msg != null) {
			if(msg.getProtocol().equals(FIPANames.InteractionProtocol.FIPA_REQUEST)) {
				try {
					System.out.println(getAgent().getName() + " received request for trm");
					ACLMessage reply = msg.createReply();
					reply.setContentObject(filterRatingsForSharing());
					reply.setPerformative(ACLMessage.INFORM);
					myAgent.send(reply);
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else if(msg.getProtocol().equals(FIPANames.InteractionProtocol.FIPA_QUERY)) {
				ACLMessage reply = msg.createReply();
				try {
					List<Rating> ratings = (List<Rating>) msg.getContentObject();
					if(ratings != null) {
						//System.out.println("Received ratings from " + msg.getSender().getLocalName());
						//ratings.forEach(r -> System.out.print(r.getValue()));
						
						if(getAgent().supernodeRatingDatabase == null) {
							getAgent().supernodeRatingDatabase = new HashMap<>();
						}
						
						getAgent().supernodeRatingDatabase.putIfAbsent(msg.getSender().getName(), new ArrayList<>());
						
						getAgent().supernodeRatingDatabase.get(msg.getSender().getName()).addAll(ratings);
						
						int nodesCount = getAgent().supportedNodes.contains(msg.getSender()) ? getAgent().supportedNodes.size() : getAgent().supportedNodes.size() + 1;  
						for(Rating r : ratings) {
							getAgent().supernodePerfomanceReport.add(r.getIteration() + ";" + nodesCount + ";" + (r.isSatisfied() ? 1 : 0));
						}
						
						/*double loss = ratings.stream().map(r -> r.getPacketLossPercentage()).reduce((l1,l2) -> {
							return (l1 + l2) / 2;
						}).get();*/
						
					}
					reply.setContent("ACK");
					reply.setPerformative(ACLMessage.INFORM);
				} catch (UnreadableException e) {
					e.printStackTrace();
				}
			}
		} else {
			block();
		}
	}
	
	private HashMap<String, List<Rating>> filterRatingsForSharing() {
		HashMap<String, List<Rating>> filteredRatings = new HashMap<>();
		
		int i = 0;
		for(String key : getAgent().supernodeRatingDatabase.keySet()) {
			if(i < 5) {
				int listSite = getAgent().supernodeRatingDatabase.size();
				filteredRatings.put(key,
						listSite > 5 ?
						getAgent().supernodeRatingDatabase.get(key).subList(listSite - 5, listSite) :
						getAgent().supernodeRatingDatabase.get(key));
				i++;
			} else {
				break;
			}
		}
		
		System.out.print("Sending certificates ");
		filteredRatings.values().forEach(v -> System.out.print(v.size()));
		System.out.println("");
		
		return filteredRatings;
	}
	
	@Override
	public SupernodeAgent getAgent() {
		return (SupernodeAgent) myAgent;
	}

}
