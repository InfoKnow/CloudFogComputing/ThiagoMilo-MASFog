package br.com.tm.node.supernode;

import java.io.IOException;

import br.com.tm.util.FogServiceModel;
import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;

public class InformCloudManAndLocalCoordBehaviour extends OneShotBehaviour {

	private static final String INFORM_NODE_ADD_TO_SUPERNODE = "inform-node-add-to-supernode";
	private static final String INFORM_NODE_REMOV_TO_SUPERNODE = "inform-node-remov-to-supernode";
	
	public static final int INFORM_NODE_ADD = 0;
	public static final int INFORM_NODE_REMOVAL = 1;
	
	private int informType;
	private AID node;
	
	public InformCloudManAndLocalCoordBehaviour(int informType, AID node) {
		this.informType = informType;
		this.node = node;
	}
	
	@Override
	public void action() {
		ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
		msg.setProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST);
		
		if(informType == INFORM_NODE_ADD) {
			msg.setConversationId(INFORM_NODE_ADD_TO_SUPERNODE);
		} else  {
			msg.setConversationId(INFORM_NODE_REMOV_TO_SUPERNODE);
		}
		
		if(getAgent().getCloudManagers() != null) {
			for(AID nodeAID : getAgent().getCloudManagers()) {
				msg.addReceiver(nodeAID);
			}
		}
		
		if((getAgent().getFogServiceModel() == FogServiceModel.FIXED || 
		    getAgent().getFogServiceModel() == FogServiceModel.PARTIALLY_OPEN) && 
			getAgent().getLocalCoordinator() != null) {
			msg.addReceiver(getAgent().getLocalCoordinator());
		}
		
		try {
			msg.setContentObject(this.node);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		myAgent.send(msg);
	}
	
	@Override
	public SupernodeAgent getAgent() {
		return (SupernodeAgent) myAgent;
	}

}
