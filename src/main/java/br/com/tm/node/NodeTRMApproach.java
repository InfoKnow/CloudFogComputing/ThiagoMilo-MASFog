package br.com.tm.node;

public enum NodeTRMApproach {

	CR_FROM_SUPERNODE(0, "inactive"),
	CR_FROM_LOCAL_COORDINATOR(1, "searching_server"),
	WR_FROM_LOCAL_COORDINATOR(2, "connecting"),
	WR_FROM_NODES(3, "ready");
	
	public int value;
	public String description;

	public static NodeTRMApproach parse(int value) {
		for(NodeTRMApproach approach : NodeTRMApproach.values()) {
			if(approach.value == value) {
				return approach;
			}
		}
		
		return null;
	}
	
	private NodeTRMApproach(int value, String description) {
		this.value = value;
		this.description = description;
	}
	
}
