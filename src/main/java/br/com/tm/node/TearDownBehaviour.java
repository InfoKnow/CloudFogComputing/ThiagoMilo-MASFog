package br.com.tm.node;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import br.com.tm.repfogagent.trm.Rating;
import br.com.tm.util.FogServiceModel;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;

public class TearDownBehaviour extends OneShotBehaviour {

	private static final long serialVersionUID = 1L;

	private static final String SUPERNODE_RATING_ID = "supernode-rating";
	private static final String RATING_TO_SUPERNODE_ID = "rating-to-supernode";
	private static final String NODE_DISCONNECT_FOG_SERVER_CONVERSATION_ID = "node-disconnect-fog-server-conversation-id";
	private static final String NODE_DISCONNECT_CLOUD_CONVERSATION_ID = "node-disconnect-cloud-conversation-id";
	private static final String SIMULATION_STATS_CONVERSATION = "sim-stats-certificate";
	
	
	boolean forGood = false;
	
	public TearDownBehaviour(boolean forGood) {
		this.forGood = forGood;
	}
	
	@Override
	public void action() {
		System.out.println("TEARING DOWN!");
		sendDataToServers();
		
		//Disconnect from fog server - it can be a supernode or cloud
		if(getAgent().server != null) {
			//Disconnect from fog server
			ACLMessage message = new ACLMessage(ACLMessage.CANCEL);
			message.setConversationId(NODE_DISCONNECT_FOG_SERVER_CONVERSATION_ID);
			message.setProtocol(FIPANames.InteractionProtocol.FIPA_SUBSCRIBE);
			message.addReceiver(((NodeAgent) myAgent).server);
			
			myAgent.send(message);
			
			System.out.println("Disconnecting from fog server");
			getAgent().traceReport.add("Disconnecting from fog server");
			
			getAgent().stopFogClient();
		}
		
		//Disconnect form the cloud server
		if(this.forGood) {
			ACLMessage message = new ACLMessage(ACLMessage.CANCEL);
			message.setConversationId(NODE_DISCONNECT_CLOUD_CONVERSATION_ID);
			message.setProtocol(FIPANames.InteractionProtocol.FIPA_SUBSCRIBE);
			
			if(getAgent().cloudManagers != null) {
				for(AID nodeAID : getAgent().cloudManagers) {
					message.addReceiver(nodeAID);
				}
			}
			myAgent.send(message);
			System.out.println("Disconnecting from cloud");
			getAgent().traceReport.add("Disconnecting from cloud");
		}
	}
	
	/**
	 * Send data to the fog server (ratings) and statistics about service to cloud server.
	 */
	private void sendDataToServers() {
		
		// Sending ratings data to supernode
		if(getAgent().serverType != NodeAgent.CLOUD) {
			try {
				ACLMessage message1 = new ACLMessage(ACLMessage.REQUEST);
				message1.setConversationId(RATING_TO_SUPERNODE_ID);
				message1.setContentObject(
					(ArrayList<Rating>)(getAgent().ratingsTransmissionCache.get(((NodeAgent) myAgent).server.getName()))
				);
				message1.setProtocol(FIPANames.InteractionProtocol.FIPA_QUERY);
				message1.addReceiver(((NodeAgent) myAgent).server);
				myAgent.send(message1);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		// Sending ratings to local coodinator - if exists
		if((getAgent().fogServiceModel == FogServiceModel.PARTIALLY_OPEN || 
		    getAgent().fogServiceModel == FogServiceModel.FIXED) && 
			getAgent().localCoordinator != null) {
			try {
				ACLMessage messageToLC = new ACLMessage(ACLMessage.REQUEST);
				messageToLC.setConversationId(SUPERNODE_RATING_ID);
				messageToLC.setContentObject(getAgent().ratingsTransmissionCache);
				messageToLC.setProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST);
				messageToLC.addReceiver(getAgent().localCoordinator);
				myAgent.send(messageToLC);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		// Sending the simulation status to cloud
		ACLMessage message2 = new ACLMessage(ACLMessage.REQUEST);
		message2.setConversationId(SIMULATION_STATS_CONVERSATION);
		message2.setProtocol(FIPANames.InteractionProtocol.FIPA_QUERY);
		if(getAgent().cloudManagers != null) {
			for(AID nodeAID : getAgent().cloudManagers) {
				message2.addReceiver(nodeAID);
			}
		}
		message2.setContent(getAgent().logCache);
		getAgent().logCache = "";
		myAgent.send(message2);
		
		getAgent().ratingsTransmissionCache = new HashMap<>();
	}
	
	@Override
	public NodeAgent getAgent() {
		return (NodeAgent) myAgent;
	}
}
