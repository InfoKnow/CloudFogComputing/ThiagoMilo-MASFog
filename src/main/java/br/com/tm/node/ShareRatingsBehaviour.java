package br.com.tm.node;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map.Entry;

import br.com.tm.repfogagent.trm.Rating;
import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;

public class ShareRatingsBehaviour extends OneShotBehaviour {

	private static final long serialVersionUID = 1L;
	
	private static final String RATING_TO_SUPERNODE_ID = "rating-to-supernode";

	private AID supernode;
	
	public ShareRatingsBehaviour(AID supernode) {
		this.supernode = supernode;
	}
	
	@Override
	public void action() {
		List<Rating> filteredRatings = getShareableRatings();
		if(filteredRatings != null && filteredRatings.size() > 0) {
			try {
				ACLMessage shareRatingsMsg = new ACLMessage(ACLMessage.QUERY_REF);
				shareRatingsMsg.setConversationId(RATING_TO_SUPERNODE_ID);
				shareRatingsMsg.setProtocol(FIPANames.InteractionProtocol.FIPA_QUERY);
				shareRatingsMsg.setContentObject(new ArrayList<>(filteredRatings));
				myAgent.send(shareRatingsMsg);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	private List<Rating> getShareableRatings() {
		NodeAgent agent = (NodeAgent) myAgent;
		if(!agent.localRatings.isEmpty() && agent.localRatings.containsKey(supernode.getName())) {
			return filterAlreadySharedRatings(supernode.getName(), agent.localRatings.get(supernode.getName()));		
		}

		return null;
	}
	
	private List<Rating> filterAlreadySharedRatings(String supernode, List<Rating> ratings) {
		NodeAgent agent = (NodeAgent) myAgent;
		List<Rating> filteredRatings = new ArrayList<>();
		if(agent.sharedRatingsMapControl.containsKey(supernode)) {
			Date lastSharedDate = agent.sharedRatingsMapControl.get(supernode);
			for(Rating rating : ratings) {
				if(rating.getDate().compareTo(lastSharedDate) > 0) {
					filteredRatings.add(rating);
				}
			}
			
			return filteredRatings;
		} else {
			agent.sharedRatingsMapControl.put(supernode, ratings.get(ratings.size() - 1).getDate());
		}
		
		return ratings;
	}
}
