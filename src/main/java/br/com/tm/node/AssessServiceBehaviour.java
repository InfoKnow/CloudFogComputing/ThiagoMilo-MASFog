package br.com.tm.node;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import br.com.tm.repfogagent.trm.Rating;
import br.com.tm.stream.MessageStats;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;

public class AssessServiceBehaviour extends TickerBehaviour {

	private static final long serialVersionUID = 1L;

	private double lossPctThreshold;
	private int ratingHistorySize;
	private int latencyRequirement;
	private int tickCount = 0;
	
	public AssessServiceBehaviour(Agent a, long period, double lossPctThreshold, int ratingHistorySize, int latencyRequirement) {
		super(a, period);
		this.lossPctThreshold = lossPctThreshold;
		this.ratingHistorySize = ratingHistorySize;
		this.latencyRequirement = latencyRequirement;
	}

	@Override
	protected void onTick() {
		tickCount++;
		updataRatingData();
		/*if(changeServer()) {
			// TODO insert logic to check the supernodes candidates list time to live
			if(((NodeAgent) myAgent).supernodesCandidatesList != null && !((NodeAgent) myAgent).supernodesCandidatesList.isEmpty()) {
				myAgent.addBehaviour(new SelectServerBehaviour(((NodeAgent) myAgent).supernodesCandidatesList));
			} else {
				myAgent.restartLater(new RequestServiceBehaviour(), 1);
			}
		}*/
	}
	
	private boolean changeServer() {
		NodeAgent agent = (NodeAgent) myAgent;
		
		if(agent.localRatings != null && agent.localRatings.containsKey(agent.server.getName()) && 
		  !agent.localRatings.get(agent.server.getName()).isEmpty() &&
		   agent.localRatings.get(agent.server.getName()).size() >= agent.supernodeCheckPoint) {
			int countGoodRatingValue = 0;
			for(Rating rating : agent.localRatings.get(agent.server.getName())) {
				if(rating.getValue() >= agent.ratingSatisfactionThreshold) {
					countGoodRatingValue++;
				}
			}
			
			if(countGoodRatingValue < agent.ratingPctSatisfactionForChange) {
				return true;
			}
		}
		
		return false;
	}
	
	private void updataRatingData() {
		//getAgent().collectData();
	}
	
	@Override
	public NodeAgent getAgent() {
		return (NodeAgent) myAgent;
	}
}
